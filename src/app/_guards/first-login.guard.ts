import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../_services/user.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirstLoginGuard implements CanActivate {
  constructor(private router: Router, private service: UserService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.service.firstLoginAccess.pipe(map(res => {
      const result = res.user.isFirstLogin;
      if (result) {

        return true;
      }
      else {
        this.router.navigate(['/home']);
        return false;
      }
    }))



    // return true;
  }

}
