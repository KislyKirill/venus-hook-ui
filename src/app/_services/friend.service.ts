import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class FriendService {

  constructor(private fb: FormBuilder, private http: HttpClient) { }
  readonly BaseURI = 'https://localhost:44327/api';

  getRequests() {
    return this.http.get(this.BaseURI + '/Friends/Requests');
  }

  getFriends() {
    return this.http.get(this.BaseURI + '/Friends');
  }

  acceptRequest(id: number) {
    return this.http.get(this.BaseURI + '/Friends/AcceptRequest/' + id);
  }

  rejectRequest(id: number) {
    return this.http.get(this.BaseURI + '/Friends/RejectRequest/' + id);
  }

  removeFriend(friendId: string) {
    return this.http.get(this.BaseURI + '/Friends/DeleteFriend/' + friendId);
  }

  detailsFriendprofile(friendId: string) {
    return this.http.get(this.BaseURI + '/Friends/FriendDetails/' + friendId);
  }

}

