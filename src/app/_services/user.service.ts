import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private fb: FormBuilder, private http: HttpClient) { }
  readonly BaseURI = 'https://localhost:44327/api';

  registerModel = this.fb.group({
    UserName: ['', Validators.required],
    Email: ['', Validators.email],
    FirstName: ['', Validators.required],
    LastName: ['', Validators.required],
    DateOfBirth: ['', Validators.required],
    GenderId: ['', Validators.required],
    Passwords: this.fb.group({
      Password: ['', [Validators.required, Validators.minLength(6)]],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.comparePasswordsRegist })
  })

  moreRegisterModel = this.fb.group({
    Interests: [''],
    TargetRelationship: [''],
    Education: [''],
    Nationality: [''],
    FinancialSituation: [''],
    BadHabits: [''],
    Languages: [''],
    Height: ['175'],
    Weight: ['95'],
    FamilyStatus: ['']
  })

  editMainModel = this.fb.group({
    UserName: ['', Validators.required],
    FirstName: ['', Validators.required],
    LastName: ['', Validators.required],
    DateOfBirth: ['', Validators.required],
    GenderId: ['', Validators.required]
  })

  editMoreModel = this.fb.group({
    Interests: [''],
    TargetRelationship: [''],
    Education: [''],
    Nationality: [''],
    FinancialSituation: [''],
    BadHabits: [''],
    Languages: [''],
    Height: [''],
    Weight: [''],
    FamilyStatus: ['']
  })

  privateModel = this.fb.group({
    IsPrivate: ['']
  })

  changePasswordModel = this.fb.group({
    OldPassword: ['', Validators.required],
    Passwords: this.fb.group({
      NewPassword: ['', [Validators.required, Validators.minLength(6)]],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.comparePasswordsChangePass })
  })

  comparePasswordsRegist(fb: FormGroup) {
    let confirmPswrdCtrl = fb.get('ConfirmPassword');

    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl) {
      if (fb.get('Password').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }
  comparePasswordsChangePass(fb: FormGroup) {
    let confirmPswrdCtrl = fb.get('ConfirmPassword');

    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl) {
      if (fb.get('NewPassword').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }

  register() {
    var body = {
      UserName: this.registerModel.value.UserName,
      Email: this.registerModel.value.Email,
      FirstName: this.registerModel.value.FirstName,
      LastName: this.registerModel.value.LastName,
      DateOfBirth: this.registerModel.value.DateOfBirth,
      GenderId: this.registerModel.value.GenderId,
      Password: this.registerModel.value.Passwords.Password,
    };
    return this.http.post(this.BaseURI + '/Users/Register', body);
  }

  moreRegister() {
    var body = {
      Interests: this.moreRegisterModel.value.Interests,
      TargetRelationship: this.moreRegisterModel.value.TargetRelationship,
      EducationId: this.moreRegisterModel.value.Education,
      NationalityId: this.moreRegisterModel.value.Nationality,
      FinancialSituationId: this.moreRegisterModel.value.FinancialSituation,
      BadHabits: this.moreRegisterModel.value.BadHabits,
      Languages: this.moreRegisterModel.value.Languages,
      Height: this.moreRegisterModel.value.Height,
      Weight: this.moreRegisterModel.value.Weight,
      FamilyStatusId: this.moreRegisterModel.value.FamilyStatus
    };
    return this.http.post(this.BaseURI + '/UserProfiles/CompletedRegister', body)
  }

  editMainInfo() {
    var body = {
      UserName: this.editMainModel.value.UserName,
      FirstName: this.editMainModel.value.FirstName,
      LastName: this.editMainModel.value.LastName,
      DateOfBirth: this.editMainModel.value.DateOfBirth,
      GenderId: this.editMainModel.value.GenderId
    };
    return this.http.put(this.BaseURI + '/UserProfiles/EditProfile', body);
  }

  editMoreInfo() {
    var body = {
      Interests: this.editMoreModel.value.Interests,
      TargetRelationship: this.editMoreModel.value.TargetRelationship,
      EducationId: this.editMoreModel.value.Education,
      NationalityId: this.editMoreModel.value.Nationality,
      FinancialSituationId: this.editMoreModel.value.FinancialSituation,
      BadHabits: this.editMoreModel.value.BadHabits,
      Languages: this.editMoreModel.value.Languages,
      Height: this.editMoreModel.value.Height,
      Weight: this.editMoreModel.value.Weight,
      FamilyStatusId: this.editMoreModel.value.FamilyStatus
    };
    return this.http.put(this.BaseURI + '/UserProfiles/EditMoreProfile', body)
  }

  privateSetting() {
    var body = {
      IsPrivate: this.privateModel.value.IsPrivate
    };
    return this.http.put(this.BaseURI + '/UserProfiles/PrivateSettings', body)
  }

  ChangePassword() {
    var body = {
      OldPassword: this.changePasswordModel.value.OldPassword,
      NewPassword: this.changePasswordModel.value.Passwords.NewPassword,
    };
    return this.http.put(this.BaseURI + '/UserProfiles/ChangePassword', body);
  }

  login(formData) {
    return this.http.post(this.BaseURI + '/Users/Login', formData);
  }

  getUserProfile() {
    return this.http.get(this.BaseURI + '/UserProfiles');
  }

  getAdditionalInfo() {
    return this.http.get(this.BaseURI + '/UserProfiles/AdditionalInfo')
  }

  isFirsrtLogin: boolean;

  get firstLoginAccess(): Observable<any> {
    return this.getUserProfile().pipe(tap(data => this.isFirsrtLogin = data.user.isFirsrtLogin));
  }

  postFile(fileToUpload: File) {
    const formData: FormData = new FormData();
    formData.append('Image', fileToUpload);
    return this.http.post(this.BaseURI + '/UserProfiles/UploadImages', formData);
  }

  getAllUserProfiles() {
    return this.http.get(this.BaseURI + '/Filters')
  }
}
