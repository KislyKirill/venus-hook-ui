import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  userDetails;

  constructor(private router: Router, private service: UserService, private toastr: ToastrService, private modalService: NgbModal) { }

  ngOnInit() {
    this.service.changePasswordModel.reset();
    this.service.getUserProfile().subscribe(
      res => {
        this.userDetails = res;
      },
      err => {
        console.log(err);
      }
    )
  }

  open(content) {
    this.modalService.open(content);
  }

  submitGeneral() {
    this.service.privateSetting().subscribe(
      res => {
        this.router.navigateByUrl('/home/settings');
        this.toastr.success('General setting is updated', 'The changes are saved');
      },
      err => {
        if (err.status == 400)
          this.toastr.error(err.error.message, 'Edit information failed');
        console.log(err);
      }
    )
  }

  submitChangePassword() {
    this.service.ChangePassword().subscribe(
      res => {
        this.ngOnInit();
        this.router.navigateByUrl('/home/settings');
        this.toastr.success('The password change is successful', 'Change password');
      },
      err => {
        this.toastr.error(err.error.message, 'Change password failed');
      }
    )
  }

}
