import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {

  searchResult;

  constructor(private router: Router, private service: UserService) { }

  ngOnInit() {
    this.service.getAllUserProfiles().subscribe(
      res => {
        this.searchResult = res;
        console.log(res);
      }
    )
  }

}
