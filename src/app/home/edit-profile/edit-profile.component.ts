import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';
import { ToastrService } from 'ngx-toastr';
import { additionalData } from 'src/app/_models/additionalData';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  userDetails;
  additionalData: additionalData;

  constructor(private router: Router, private service: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.service.getUserProfile().subscribe(
      res => {
        this.userDetails = res;
      },
      err => {
        console.log(err);
      }
    )

    this.service.getAdditionalInfo().subscribe(
      res => {
        this.additionalData = res;
        console.log(res);
      }
    )
  }

  onSubmitMainInfo() {
    this.service.editMainInfo().subscribe(
      (res: any) => {
        this.router.navigateByUrl('/home/edit-profile');
        this.toastr.success('The new data will be reflected on Your page.', 'The changes are saved');
      },
      err => {
        if (err.error.DateOfBirth)
          this.toastr.error('Choose a date of birth between 16 and 130.', 'Edit information failed')
        else
          this.toastr.error(err.error.message, 'Edit information failed')
        console.log(err);
      }
    )
  }

  onSubmitMoreInfo() {
    this.service.editMoreInfo().subscribe(
      (res: any) => {
        this.router.navigateByUrl('/home/edit-profile');
        this.toastr.success('The new data will be reflected on Your page.', 'The changes are saved');
      },
      err => {
        if (err.status == 400)
          this.toastr.error('Additional information has been added', 'Edit information failed');
        console.log(err);
      }
    )
  }

}
