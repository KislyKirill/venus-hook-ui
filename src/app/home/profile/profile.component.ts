import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services/user.service';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NullTemplateVisitor } from '@angular/compiler';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  userDetails;
  imageUrl: string = "/assets/img/DefaultAvatar.png";
  fileToUpload: File = null;

  constructor(private router: Router, private service: UserService, private toastr: ToastrService, private modalService: NgbModal) { }

  ngOnInit() {
    this.service.getUserProfile().subscribe(
      res => {
        this.userDetails = res;
      },
      err => {
        console.log(err);
      }
    );
  }

  onEditProfile() {
    this.router.navigate(['/home/edit-profile']);
  }

  open(content) {
    this.modalService.open(content);
  }

  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    // Show image preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  OnChangePhoto(Image) {
    this.service.postFile(this.fileToUpload).subscribe(
      data => {
        this.ngOnInit();
        console.log('succes upload');
        Image.value = null;
        this.toastr.success('You have added his avatar', 'Picture uploaded');
      },
      err => {
        this.toastr.error(err.error.message, 'Picture uploaded failed')
      }
    );
  }
}
