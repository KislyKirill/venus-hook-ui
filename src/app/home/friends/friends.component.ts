import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FriendService } from 'src/app/_services/friend.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  friendList;
  userId: string;

  constructor(private router: Router,
    private activateRoute: ActivatedRoute,
    private service: FriendService,
    private toasts: ToastrService) { }

  async ngOnInit() {
    this.service.getFriends().subscribe(
      res => {
        this.friendList = res;
      }
    )

    await this.activateRoute.params.subscribe(params => this.userId = params.id);
  }

  onRemoveFriend(friendId: string) {
    this.service.removeFriend(friendId).subscribe(
      async (res: any) => {
        this.ngOnInit();
        this.toasts.success('You have removed the user from your friends', 'Friend removed to requests')
      },
      err => {
        console.log(err);
      }
    )
  }

  onUserProfile(userProfileId: string) {
    this.router.navigateByUrl('/home/user-profile' + this.userId);
  }
}
