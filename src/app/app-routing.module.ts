import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { ProfileComponent } from './home/profile/profile.component';
import { MoreinfoComponent } from './moreinfo/moreinfo.component';
import { FirstLoginGuard } from './_guards/first-login.guard';
import { EditProfileComponent } from './home/edit-profile/edit-profile.component';
import { SettingsComponent } from './home/settings/settings.component';
import { MatchesComponent } from './home/matches/matches.component';
import { FriendsComponent } from './home/friends/friends.component';
import { BlacklistComponent } from './home/friends/blacklist/blacklist.component';
import { RequestsComponent } from './home/friends/requests/requests.component';

const routes: Routes = [
  { path: '', redirectTo: '/user/login', pathMatch: 'full' },
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'registration', component: RegistrationComponent },
      { path: 'login', component: LoginComponent }
    ]
  },
  { path: 'moreinfo', component: MoreinfoComponent, canActivate: [FirstLoginGuard] },
  {
    path: 'home', component: HomeComponent, canActivate: [AuthGuard],
    children: [
      { path: 'profile', component: ProfileComponent },
      { path: 'edit-profile', component: EditProfileComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'matches', component: MatchesComponent },
      { path: 'friends', component: FriendsComponent },
      { path: 'friends/blacklist', component: BlacklistComponent },
      { path: 'friends/requests', component: RequestsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
