import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';
import { ToastrService } from 'ngx-toastr';
import { additionalData } from '../_models/additionalData';

@Component({
  selector: 'app-moreinfo',
  templateUrl: './moreinfo.component.html',
  styleUrls: ['./moreinfo.component.css']
})
export class MoreinfoComponent implements OnInit {

  additionalData: additionalData;

  constructor(private router: Router, private service: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.service.getAdditionalInfo().subscribe(
      res => {
        this.additionalData = res as additionalData;
        console.log(res as additionalData);
      }
    )
  }

  onSubmit() {
    this.service.moreRegister().subscribe(
      res => {
        this.router.navigateByUrl('/home');
        this.toastr.success('You have completed the registration.', 'Registration is completed');
        //this.ngOnInit();
        console.log("updated");
      },
      err => {
        if (err.status == 400)
          this.toastr.error('Additional information has been added', 'Registration is failed');
        //else
        console.log(err);
      }
    )
  }

  onLogout() {
    localStorage.removeItem('token');
    this.router.navigate(['/user/login']);
  }
}