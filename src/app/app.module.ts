import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { from } from 'rxjs';
import { UserService } from './_services/user.service';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthInterceptor } from './_guards/auth.interceptor';
import { ProfileComponent } from './home/profile/profile.component';
import { MoreinfoComponent } from './moreinfo/moreinfo.component';
import { EditProfileComponent } from './home/edit-profile/edit-profile.component';
import { SettingsComponent } from './home/settings/settings.component';
import { MatchesComponent } from './home/matches/matches.component';
import { FriendsComponent } from './home/friends/friends.component';
import { BlacklistComponent } from './home/friends/blacklist/blacklist.component';
import { RequestsComponent } from './home/friends/requests/requests.component';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    ProfileComponent,
    MoreinfoComponent,
    EditProfileComponent,
    SettingsComponent,
    MatchesComponent,
    FriendsComponent,
    BlacklistComponent,
    RequestsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    NgbModule
  ],
  providers: [
    UserService, {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    // NgbdModalConfirmAutofocus
  ]
})
export class AppModule { }
