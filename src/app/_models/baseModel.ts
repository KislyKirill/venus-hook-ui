export class baseModel {
    constructor(public id: number = null, public value: string = null) { }
}