import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services/user.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styles: []
})
export class RegistrationComponent implements OnInit {
  public groupForm: FormGroup;

  constructor(public service: UserService, private toastr: ToastrService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.service.registerModel.reset();
  }

  onSubmit() {
    this.service.register().subscribe(
      (res: any) => {
        if (res.succeeded) {
          this.service.registerModel.reset();
          this.toastr.success('Confirm your email', 'Registration successful');
          this.router.navigate(['/moreinfo']);
        } else {
          res.errors.forEach(element => {
            switch (element.code) {
              case 'DuplicateUserName':
                this.toastr.error('Username is already taken', 'Registration failed');
                break;

              default:
                this.toastr.error(element.description, 'Registration failed');
                break;
            }
          });
        }
      },
      err => {
        if (err.status == 400) {
          if (err.error.DateOfBirth)
            this.toastr.error('Choose a date of birth between 16 and 130.', 'Registration failed')
          if (err.error.Gender)
            this.toastr.error('Select your gender.', 'Registration failed')
        }
        else
          console.log(err);
      }
    );
  }
}
